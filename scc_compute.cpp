#include "scc_compute.hpp"

SCCCompute::SCCCompute(Graph& g, std::ostream& ls, std::string lp)
    : graph(g), log_stream(ls), log_prefix(lp) {}

void SCCCompute::PrintDebugMessage(std::string m, std::string fn) {
    log_stream << log_prefix << "::" << fn << "() : " << m << std::endl;
}