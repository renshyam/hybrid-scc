SHELL = /bin/bash
CXXFLAGS = -std=c++11
NVCC = nvcc

all: runner.out

runner.out: runner.cpp timer.o graph.o scc_compute.o tarjan_scc_compute.o
	$(CXX) $(CXXFLAGS) -o $@ $^

tarjan_scc_compute.o: tarjan_scc_compute.cpp tarjan_scc_compute.hpp scc_compute.hpp
	$(CXX) $(CXXFLAGS) -c $^

scc_compute.o: scc_compute.cpp scc_compute.hpp graph.hpp
	$(CXX) $(CXXFLAGS) -c $^

graph.o: graph.cpp graph.hpp
	$(CXX) $(CXXFLAGS) -c $^

timer.o: timer.cpp timer.hpp
	$(CXX) $(CXXFLAGS) -c $^

run: runner.out data/input.txt
	./runner.out -i data/input.txt -v 7115 -e 103689 -o output/out.txt

data/input.txt:
	mkdir -p data
	wget -O $@.gz https://snap.stanford.edu/data/wiki-Vote.txt.gz
	gunzip $@.gz

.PHONY: clean

clean:
	rm -f *.gch
	rm -f *.out
	rm -f *.o
	rm -rf output/*
