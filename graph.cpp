#include "graph.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>

Graph::Graph(int v, int e, std::ifstream &file)
    : num_vertices(v), num_edges(e) {
    LoadFromFile(file);
}

int Graph::GetNumberOfVertices() { return num_vertices; }

int Graph::GetNumberOfEdges() { return num_edges; }

int Graph::GetVertexPosition(long long id) {
    int pos = GetVertexPositionUnsafe(id);
    if (pos == -1) {
        throw std::invalid_argument(
            "Graph::GetVertexPosition() : Vertex with ID not found");
    }
    return pos;
}

Range Graph::GetEdgeRange(int vertex) {
    if (vertex < 0 || vertex >= num_vertices) {
        throw std::out_of_range(
            "Graph::GetEdgeRange() : Vertex index out of range");
    }
    int start = vertex_offset[vertex];
    int end =
        (vertex == num_vertices - 1) ? num_edges : vertex_offset[vertex + 1];
    return {start, end};
}

long long Graph::GetEdgeDest(int index) {
    if (index < 0 || index >= num_edges) {
        throw std::out_of_range(
            "Graph::GetEdgeDest() : Edge index out of range");
    }
    return adj_list[index];
}

const int *Graph::GetVertexOffsets() { return vertex_offset.data(); }

const int *Graph::GetAdjacencyList() { return adj_list.data(); }

Graph::~Graph() {
    vertex_offset.clear();
    adj_list.clear();
}

int Graph::GetVertexPositionUnsafe(long long id) {
    VertexPosMap::iterator it = vertex_pos.find(id);
    if (it == vertex_pos.end()) {
        return -1;
    }
    return it->second;
}

void Graph::LoadFromFile(std::ifstream &file) {
    vertex_pos.clear();
    vertex_offset.clear();
    adj_list.clear();
    vertex_pos.reserve(num_vertices);
    vertex_offset.reserve(num_vertices);
    adj_list.reserve(num_edges);
    VertexAdjacencyList vertex_adj_list;
    vertex_adj_list.resize(num_vertices);
    PopulateDataFromTxtFile(file, vertex_adj_list);
    if (file.bad()) {
        throw std::ios_base::failure(
            "Graph::LoadFromTxtFile() : Error reading file");
    }
    int cur_vertex_offset = 0;
    for (int v = 0; v < num_vertices; v++) {
        vertex_offset.push_back(cur_vertex_offset);
        cur_vertex_offset += vertex_adj_list[v].size();
        std::copy(vertex_adj_list[v].begin(), vertex_adj_list[v].end(),
                  std::back_inserter(adj_list));
    }
}

void Graph::PopulateDataFromTxtFile(std::ifstream &file,
                                    VertexAdjacencyList &vertex_adj_list) {
    std::string line;
    int cur_vertex_id = 0;
    while (std::getline(file, line)) {
        std::stringstream stream(line);
        char c;
        stream >> c;
        if (c == '#') {
            continue;
        }
        stream.clear();
        stream.seekg(0, std::ios::beg);
        long long source, dest;
        stream >> source >> dest;
        int source_pos = GetVertexPositionUnsafe(source);
        if (source_pos == -1) {
            source_pos = cur_vertex_id++;
            vertex_pos[source] = source_pos;
        }
        int dest_pos = GetVertexPositionUnsafe(dest);
        if (dest_pos == -1) {
            dest_pos = cur_vertex_id++;
            vertex_pos[dest] = dest_pos;
        }
        vertex_adj_list[source_pos].push_back(dest_pos);
    }
    if (cur_vertex_id != num_vertices) {
        throw std::out_of_range(
            "Graph::PopulateDataFromTxtFile() : Mismatch in number of "
            "vertices");
    }
}
