#include "scc_compute.hpp"

class TarjanSCCCompute : public SCCCompute {
   public:
    TarjanSCCCompute(Graph& g, std::ostream& ls);
    void Compute(SCCSolution& ans);
};