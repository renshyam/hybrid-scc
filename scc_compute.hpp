#include <unordered_map>
#include <vector>
#ifndef GRAPH_H
#include "graph.hpp"
#endif

typedef std::unordered_map<long long, int> SCCSolution;

class SCCCompute {
   public:
    SCCCompute(Graph& g, std::ostream& ls, std::string lp);
    virtual void Compute(SCCSolution& ans) = 0;

   protected:
    void PrintDebugMessage(std::string m, std::string fn);
    Graph& graph;
    std::ostream& log_stream;
    std::string log_prefix;
};