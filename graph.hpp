#define GRAPH_H

#include <fstream>
#include <unordered_map>
#include <vector>

typedef std::pair<int, int> Range;

class Graph {
   public:
    Graph(int v, int e, std::ifstream &file);
    int GetNumberOfVertices();
    int GetNumberOfEdges();
    int GetVertexPosition(long long id);
    Range GetEdgeRange(int vertex);
    long long GetEdgeDest(int index);
    const int *GetVertexOffsets();
    const int *GetAdjacencyList();
    ~Graph();

   private:
    typedef std::vector<std::vector<int>> VertexAdjacencyList;
    typedef std::unordered_map<long long, int> VertexPosMap;
    int GetVertexPositionUnsafe(long long id);
    void LoadFromFile(std::ifstream &file);
    void PopulateDataFromTxtFile(std::ifstream &file,
                                 VertexAdjacencyList &vertex_adj_list);
    int num_vertices;
    int num_edges;
    VertexPosMap vertex_pos;
    std::vector<int> vertex_offset;
    std::vector<int> adj_list;
};
