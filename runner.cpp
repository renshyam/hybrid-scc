#include <unistd.h>
#include <fstream>
#include <iostream>
#include "graph.hpp"
#include "tarjan_scc_compute.hpp"

int main(int argc, char **argv) {
    std::string input_file_name, output_file_name, log_file_name;
    int num_vertices = -1, num_edges = -1;
    int cmd_flag;
    while ((cmd_flag = getopt(argc, argv, "i:v:e:o:l:")) != -1) {
        switch (cmd_flag) {
            case 'i':
                input_file_name.assign(optarg);
                break;
            case 'v':
                num_vertices = std::stoi(std::string(optarg));
                break;
            case 'e':
                num_edges = std::stoi(std::string(optarg));
                break;
            case 'o':
                output_file_name.assign(optarg);
                break;
            case 'l':
                log_file_name.assign(optarg);
                break;
            default:
                std::cerr << "main() : Invalid command line argument"
                          << std::endl;
                exit(1);
        }
    }
    if (num_vertices == -1 || num_edges == -1) {
        std::cerr << "main() : Number of vertices and edges must be provided "
                     "(use -v -e flags)"
                  << std::endl;
        exit(1);
    }
    if (input_file_name.empty() || output_file_name.empty()) {
        std::cerr << "main() : Input and output files must be provided "
                     "(use -i -o flags)"
                  << std::endl;
        exit(1);
    }
    std::ifstream input_file(input_file_name);
    std::ofstream output_file(output_file_name), log_file_t(log_file_name);
    if (!input_file || !output_file) {
        throw std::ios_base::failure(
            "main() : Error opening one or more files");
    }
    std::ostream &log_file = !log_file_t ? std::cout : log_file_t;
    Graph graph(num_vertices, num_edges, input_file);
    SCCSolution ans;
    ans.reserve(num_vertices);
    SCCCompute *compute = new TarjanSCCCompute(graph, log_file);
    compute -> Compute(ans);
    return 0;
}